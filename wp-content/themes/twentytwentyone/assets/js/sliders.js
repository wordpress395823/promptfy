(function ($, jQuery) {
  $(".home-page .products-carousel").owlCarousel({
    loop: false,
    margin: 20,
    nav: true,
    dots:false,
    center: false,
    items: 3,
    responsive: {
      1500: {
        items: 4,
      },
      1300: {
        items: 3,
      },
      800: {
        items: 2,
      },
      500: {
        items: 2,
      },
      0: {
        items: 1,
      },
    },
  });
})(jQuery, jQuery);

(function ($, jQuery) {
  $(".related.products ul.products").owlCarousel({
    loop: false,
    margin: 20,
    nav: false,
    dots:false,
    center: false,
    items: 5,
    responsive: {
      1300: {
        items: 5,
      },
      1052: {
        items: 4,
      },
      1050: {
        items: 4,
      },
      800: {
        items: 4,
      },
      500: {
        items:3,
      },
      450: {
        items: 2,
      },
      0: {
        items: 1,
      },
    },
  });
})(jQuery, jQuery);

(function ($, jQuery) {
  $(".home-page .best-rated-carousel").owlCarousel({
    loop: false,
    margin: 20,
    nav: true,
    dots: false,
    center: false,
    items: 6,
    responsive: {
      1500: {
        items: 6,
      },
      1302: {
        items: 5,
      },
      1050: {
        items: 4,
      },
      800: {
        items: 3,
      },
      500: {
        items: 2,
      },
      0: {
        items: 1,
      },
    },
  });
})(jQuery, jQuery);

(function ($, jQuery) {
  $(".single-product .tabs-carousel").owlCarousel({
    loop: false,
    margin: 10,
    nav: false,
    dots: false,
    center: false,
    items: 10,
    autoWidth: true,
  });
})(jQuery, jQuery);

(function ($, jQuery) {
  $(".home-page .tabs-carousel").owlCarousel({
    loop: false,
    margin: 10,
    nav: false,
    dots: false,
    center: false,
    items: 4,
    autoWidth: true,
  });
})(jQuery, jQuery);

(function ($, jQuery) {
  $("#wcfmmp-store .tabs-carousel").owlCarousel({
    loop: false,
    margin: 10,
    nav: false,
    dots: false,
    center: false,
    items: 4,
    autoWidth: true,
  });
})(jQuery, jQuery);

(function ($, jQuery) {
  $(".steps-carousel").owlCarousel({
    loop: false,
    margin: 20,
    nav: false,
    dots:false,
    center: false,
    items: 5,
    responsive: {
      1350: {
        items: 5,
      },
      1200: {
        items: 4,
      },
      700: {
        items: 3,
      },
      400: {
        items: 2,
      },
      0: {
        items: 1,
      },
    },
  });
})(jQuery, jQuery);

(function ($, jQuery) {
  $(".enginners-carousel").owlCarousel({
    loop: false,
    margin: 20,
    nav: false,
    dots:false,
    center: false,
    items: 4,
    responsive: {
      1300: {
        items: 4,
      },
      900: {
        items: 3,
      },
      600: {
        items: 2,
      },
      0: {
        items: 1,
      },
    },
  });
})(jQuery, jQuery);
