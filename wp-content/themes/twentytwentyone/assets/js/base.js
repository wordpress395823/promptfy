//add classe de botões em elementos
(function ($, jQuery) {

  //Rolar até produtos no clique de link
  $(document).ready(function() {
    $(".presentation a.view-more").on("click", function() {
  
      const target = $(this).attr("href");
      const targetOffset = $(target).offset().top;
  
      $("html, body").animate({
        scrollTop: targetOffset
      }, 800);
    });
  });

  $(document).ready(function() {
    // check the options of product types
    $('#is_virtual').prop('checked', true);
    $('#is_downloadable').prop('checked', true);
    $('#is_catalog').prop('checked', false);

    // set minimal regular price to 3.90
    $( '#regular_price' ).focusout( function () {
      if ( $(this).val() < 3.90 ) { 
        alert('Please set price greater than $ 3.90');
        $(this).val(3.90);
        $(this).focus();
      }
    } ); 
  });

  //Abrir tabs de produtos (alternar entre carrosséis)
  const productPrimary = document.querySelector(".products.filter .products-carousel.btw");
  const tabBtn = document.querySelectorAll(".products.filter .tab");
  const product = document.querySelectorAll(".products.filter .products-carousel");

  $(productPrimary).addClass("active");

  tabBtn.forEach((tab) => {
    tab.addEventListener("click", () => {
      const target = tab.dataset.filter;

      product.forEach((tabContent) => {
        tabContent.classList.remove("active");
      });
      const productActive = document.querySelectorAll(".products.filter .products-carousel."+ target);
      productActive.forEach((tabActive) => {
        tabActive.classList.add("active");
      });

      tabBtn.forEach((tab) => {
        tab.classList.remove("active");
      });

      tab.classList.add("active");
    });
  });

  //abrir campo de busca header mobile
  const searchBtn = $(".search-form");
  const searchClose = $('<a href="#" class="close-search">Cancel</a>');
  $(document).ready(function() {
    searchBtn.append(searchClose);
    openSearch();
  });
  $( window ).on( "resize", function() {
    openSearch();
  });
  
  function openSearch() { 
    if (window.matchMedia("(max-width: 500px)").matches) {
      searchBtn.on("click", function() {
        $("body").addClass("search-open");
      });
    
      $(document).on("click", ".search-form .close-search", function(event) {
        event.preventDefault();
        $("body").removeClass("search-open");
      });
    }
  };

  //abrir nav user mobile
  const navLi = $(".woocommerce-MyAccount-navigation-link--dashboard");

  navLi.on("click", function() {
    event.preventDefault();
    if (window.matchMedia("(max-width: 800px)").matches) {
      $("body").toggleClass("nav-open");
      addTopNav();
    }
  });
  $( window ).on( "resize", function() {
    addTopNav();
  });

  function addTopNav() {
    var div = $(".woocommerce-MyAccount-navigation");

    var lis = div.find("li");
    var alturaTotal = 0;

    lis.each(function(index) {
      var li = $(this);
      var altura = li.height();

      if (index >= 2) {
        alturaTotal += altura;
        var novoTop = "calc(100% + " + alturaTotal + "px - 1px)";
        li.css("top", novoTop);
      } 
    });
  }
  
  
  //abrir accordion
  // Menu accordion
  const accordionBtn = $("li.menu-item-has-children");
  accordionBtn.on("click", function() {
    $(this).toggleClass("open");
  });
  //main accordion
  $(".accordion").on("click", function() {
    $(this).toggleClass("open");
  });

  //abrir menu mobile 
  const menuBtn = $(".menu-button-container button");
  $(menuBtn).on("click", function() { 
    $("body").toggleClass("primary-navigation-open");
  });

  //add placeholder
  $("#regular_price").attr("placeholder", "0.00");
  $("#sale_price").attr("placeholder", "0.00");

  $("button[type='submit']").on("click", function() {
    console.log("click");
  });

})(jQuery, jQuery);