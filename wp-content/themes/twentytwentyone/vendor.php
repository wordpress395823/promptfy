<?php

/**
 * The template for displaying all single posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package WordPress
 * @subpackage Twenty_Twenty_One
 * @since Twenty Twenty-One 1.0
 * Template Name: Vendor
 */

get_header();

?>

<div class="vendor-page custom-page">
    <section class="presentation-wrap">
        <div class="presentation">
            <div class="title-wrap">
                <h1>Fuel the future of AI: Sell your prompts now</h1>
            </div>
            <p>Unleash your creativity, earn from your prompts, and shape the future of AI on our platform.</p>
            <div class="button-wrap">
                <a href="<?php echo home_url() ?>/vendor-register" class="main-btn">Create account</a>
                <a href="#view-more" class="main-btn alt view-more">View more</a>
            </div>
        </div>
    </section>
    <section class="vendor-steps" id="view-more">
        <div class="steps-carousel">
            <div class="step-block">
                <div class="step-number"><h2>01</h2></div>
                <div class="step-img">
                    <img src="<?php echo get_template_directory_uri(); ?>/assets/images/person-icon.svg" alt="">
                </div>
                <div class="step-text">
                    <p>Create your account</p>
                    <span>Fill in your personal data to <a href="<?php echo home_url() ?>/vendor-register"> create a seller account</a>.</span>
                </div>
            </div>
            <div class="step-block">
                <div class="step-number"><h2>02</h2></div>
                <div class="step-img">
                    <img src="<?php echo get_template_directory_uri(); ?>/assets/images/store-icon.svg" alt="">
                </div>
                <div class="step-text">
                    <p>Create your store</p>
                    <span>After entering your user data, create your store on the platform.</span>
                </div>
            </div>
            <div class="step-block">
                <div class="step-number"><h2>03</h2></div>
                <div class="step-img">
                    <img src="<?php echo get_template_directory_uri(); ?>/assets/images/block-icon.svg" alt="">
                </div>
                <div class="step-text">
                    <p>Upload your prompts</p>
                    <span>By creating a store, you are now ready to start publishing prompts.</span>
                </div>
            </div>
            <div class="step-block">
                <div class="step-number"><h2>04</h2></div>
                <div class="step-img">
                    <img src="<?php echo get_template_directory_uri(); ?>/assets/images/money-icon.svg" alt="">
                </div>
                <div class="step-text">
                    <p>Sell easily</p>
                    <span>Have your prompts cataloged and sold securely.</span>
                </div>
            </div>
            <div class="step-block">
                <div class="step-number"><h2>05</h2></div>
                <div class="step-img">
                    <img src="<?php echo get_template_directory_uri(); ?>/assets/images/graphic-icon.svg" alt="">
                </div>
                <div class="step-text">
                    <p>Make money</p>
                    <span>Receive 80% of the value of your sale transparently and quickly.</span>
                </div>
            </div>
        </div>
    </section>
    <section class="banner big call-to-login">
        <div class="banner-content">
            <div class="banner-text">
                <h2>Join Promptfy today and turn your creativity into a business.</h2>
                <p>At Promptfy, we recognize the power of a great prompt to unleash creativity and unlock the imagination of writers. That’s why we are committed to providing a space where you can share your ideas and monetize your talent.</p>
            </div>
            <div class="button-wrap">
                <a href="<?php echo home_url() ?>/vendor-register" class="main-btn darker big">Create account</a>
            </div>
        </div>
    </section>
	<!-- original code sellers list -->
    <!-- <section class="store-list-explore">
    	<div class="title-list">
    		<h3>Our enginners</h3>
    	</div>
    	<div class="stores-explore enginners-carousel">
			<div class="enginners-wrap-itens">
    			<a class="store-explore-block" href="">
    				<div class="store-img">
    					<img src="<?php echo get_template_directory_uri(); ?>/assets/images/store-one.png" alt="">
    				</div>
    				<div class="store-description">
    					<h4 class="store-name">@Promptbeast</h4>
    					<div class="store-tags">
    						<div class="tag rating">
    							<div class="star-icon"></div>
    							<span>4,0</span>
    						</div>
    						<div class="tag-prompts">
    							<span>5 prompts</span>
    						</div>
    					</div>
    				</div>
    			</a>
    			<a class="store-explore-block" href="">
    			<div class="store-img">
    				<img src="<?php echo get_template_directory_uri(); ?>/assets/images/store-two.png" alt="">
    			</div>
    			<div class="store-description">
    				<h4 class="store-name">@taniyoda</h4>
    				<div class="store-tags">
    					<div class="tag rating">
    						<div class="star-icon"></div>
    						<span>4,5</span>
    					</div>
    					<div class="tag-prompts">
    						<span>12 prompts</span>
    					</div>
    				</div>
    			</div>
    			</a>
			</div>
			<div class="enginners-wrap-itens">
    			<a class="store-explore-block" href="">
    			<div class="store-img">
    				<img src="<?php echo get_template_directory_uri(); ?>/assets/images/store-three.png" alt="">
    			</div>
    			<div class="store-description">
    				<h4 class="store-name">@wizardlyprompts</h4>
    				<div class="store-tags">
    					<div class="tag rating">
    						<div class="star-icon"></div>
    						<span>4,3</span>
    					</div>
    					<div class="tag-prompts">
    						<span>16 prompts</span>
    					</div>
    				</div>
    			</div>
    			</a>
    			<a class="store-explore-block" href="">
    			<div class="store-img">
    				<img src="<?php echo get_template_directory_uri(); ?>/assets/images/store-four.png" alt="">
    			</div>
    			<div class="store-description">
    				<h4 class="store-name">@flexai</h4>
    				<div class="store-tags">
    					<div class="tag rating">
    						<div class="star-icon"></div>
    						<span>3,0</span>
    					</div>
    					<div class="tag-prompts">
    						<span>19 prompts</span>
    					</div>
    				</div>
    			</div>
    			</a>
			</div>
			<div class="enginners-wrap-itens">
    			<a class="store-explore-block" href="">
    			<div class="store-img">
    				<img src="<?php echo get_template_directory_uri(); ?>/assets/images/store-five.png" alt="">
    			</div>
    			<div class="store-description">
    				<h4 class="store-name">@thecorrection</h4>
    				<div class="store-tags">
    					<div class="tag rating">
    						<div class="star-icon"></div>
    						<span>3,8</span>
    					</div>
    					<div class="tag-prompts">
    						<span>32 prompts</span>
    					</div>
    				</div>
    			</div>
    			</a>
    			<a class="store-explore-block" href="">
    			<div class="store-img">
    				<img src="<?php echo get_template_directory_uri(); ?>/assets/images/store-one.png" alt="">
    			</div>
    			<div class="store-description">
    				<h4 class="store-name">@Promptbeast</h4>
    				<div class="store-tags">
    					<div class="tag rating">
    						<div class="star-icon"></div>
    						<span>4,0</span>
    					</div>
    					<div class="tag-prompts">
    						<span>5 prompts</span>
    					</div>
    				</div>
    			</div>
    			</a>
			</div>
			<div class="enginners-wrap-itens">
    			<a class="store-explore-block" href="">
    			<div class="store-img">
    				<img src="<?php echo get_template_directory_uri(); ?>/assets/images/store-two.png" alt="">
    			</div>
    			<div class="store-description">
    				<h4 class="store-name">@taniyoda</h4>
    				<div class="store-tags">
    					<div class="tag rating">
    						<div class="star-icon"></div>
    						<span>4,5</span>
    					</div>
    					<div class="tag-prompts">
    						<span>12 prompts</span>
    					</div>
    				</div>
    			</div>
    			</a>
    			<a class="store-explore-block" href="">
    			<div class="store-img">
    				<img src="<?php echo get_template_directory_uri(); ?>/assets/images/store-three.png" alt="">
    			</div>
    			<div class="store-description">
    				<h4 class="store-name">@wizardlyprompts</h4>
    				<div class="store-tags">
    					<div class="tag rating">
    						<div class="star-icon"></div>
    						<span>4,3</span>
    					</div>
    					<div class="tag-prompts">
    						<span>16 prompts</span>
    					</div>
    				</div>
    			</div>
    			</a>
			</div>
			<div class="enginners-wrap-itens">
    			<a class="store-explore-block" href="">
    			<div class="store-img">
    				<img src="<?php echo get_template_directory_uri(); ?>/assets/images/store-four.png" alt="">
    			</div>
    			<div class="store-description">
    				<h4 class="store-name">@flexai</h4>
    				<div class="store-tags">
    					<div class="tag rating">
    						<div class="star-icon"></div>
    						<span>3,0</span>
    					</div>
    					<div class="tag-prompts">
    						<span>19 prompts</span>
    					</div>
    				</div>
    			</div>
    			</a>
    			<a class="store-explore-block" href="">
    			<div class="store-img">
    				<img src="<?php echo get_template_directory_uri(); ?>/assets/images/store-five.png" alt="">
    			</div>
    			<div class="store-description">
    				<h4 class="store-name">@thecorrection</h4>
    				<div class="store-tags">
    					<div class="tag rating">
    						<div class="star-icon"></div>
    						<span>3,8</span>
    					</div>
    					<div class="tag-prompts">
    						<span>32 prompts</span>
    					</div>
    				</div>
    			</div>
    			</a>
			</div>
			<div class="enginners-wrap-itens">
    			<a class="store-explore-block" href="">
    			<div class="store-img">
    				<img src="<?php echo get_template_directory_uri(); ?>/assets/images/store-one.png" alt="">
    			</div>
    			<div class="store-description">
    				<h4 class="store-name">@Promptbeast</h4>
    				<div class="store-tags">
    					<div class="tag rating">
    						<div class="star-icon"></div>
    						<span>4,0</span>
    					</div>
    					<div class="tag-prompts">
    						<span>5 prompts</span>
    					</div>
    				</div>
    			</div>
    			</a>
    			<a class="store-explore-block" href="">
    			<div class="store-img">
    				<img src="<?php echo get_template_directory_uri(); ?>/assets/images/store-two.png" alt="">
    			</div>
    			<div class="store-description">
    				<h4 class="store-name">@taniyoda</h4>
    				<div class="store-tags">
    					<div class="tag rating">
    						<div class="star-icon"></div>
    						<span>4,5</span>
    					</div>
    					<div class="tag-prompts">
    						<span>12 prompts</span>
    					</div>
    				</div>
    			</div>
    			</a>
			</div>
    	</div>
    </section>	-->
	<section class="my-widget-area store-list-explore">
    	<?php dynamic_sidebar( 'my-widget-area' ); ?>
	</section>
    <section class="faq-vendor">
        <div class="container-small acc-group">
			<h2>Frequently asked questions</h2>
			<div class="accordion">
				<div class="acc-wrap">
                    <p>What products or services can I advertise?</p>
                    <div class="open-acc"></div>
                </div>
				<div class="acc-content">
					<span>Exercise your creativity! Here, you can advertise anything related to Artificial Intelligence, including prompts, custom models, and specialized services.</span>
				</div>
			</div>
			<div class="accordion">
				<div class="acc-wrap">
                    <p>What earning potential can I achieve?</p>
                    <div class="open-acc"></div>
                </div>
				<div class="acc-content">
					<span>Exercise your creativity! Here, you can advertise anything related to Artificial Intelligence, including prompts, custom models, and specialized services.</span>
				</div>
			</div>
			<div class="accordion">
				<div class="acc-wrap">
                    <p>Are there costs or fees to use this platform?</p>
                    <div class="open-acc"></div>
                </div>
				<div class="acc-content">
					<span>Exercise your creativity! Here, you can advertise anything related to Artificial Intelligence, including prompts, custom models, and specialized services.</span>
				</div>
			</div>
			<div class="accordion">
				<div class="acc-wrap">
                    <p>What time investment is required for this activity?</p>
                    <div class="open-acc"></div>
                </div>
				<div class="acc-content">
					<span>Exercise your creativity! Here, you can advertise anything related to Artificial Intelligence, including prompts, custom models, and specialized services.</span>
				</div>
			</div>
			<div class="accordion">
				<div class="acc-wrap">
                    <p>How should I price my products or services?</p>
                    <div class="open-acc"></div>
                </div>
				<div class="acc-content">
					<span>Exercise your creativity! Here, you can advertise anything related to Artificial Intelligence, including prompts, custom models, and specialized services.</span>
				</div>
			</div>
			<div class="accordion">
				<div class="acc-wrap">
                    <p>How and when do I receive payments?</p>
                    <div class="open-acc"></div>
                </div>
				<div class="acc-content">
					<span>Exercise your creativity! Here, you can advertise anything related to Artificial Intelligence, including prompts, custom models, and specialized services.</span>
				</div>
			</div>
        </div>
    </section>
    <section class="cta-create-acc">
        <div class="container-small">
            <p>Create your own prompt shop on Promptfy and earn money while contributing to AI development. Join our community of sellers today and showcase your expertise!</p>
            <div class="button-wrap">
                <a href="<?php echo home_url() ?>/vendor-register" class="main-btn">Create account</a>
                <button class="main-btn alt">Send message</button>
            </div>
        </div>
    </section>
</div>
<?php
get_footer();
?>