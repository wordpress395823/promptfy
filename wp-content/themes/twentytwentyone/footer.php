<?php

/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package WordPress
 * @subpackage Twenty_Twenty_One
 * @since Twenty Twenty-One 1.0
 */

?>
</main><!-- #main -->
</div><!-- #primary -->
</div><!-- #content -->

<?php get_template_part('template-parts/footer/footer-widgets'); ?>

<footer id="colophon" class="site-footer">

	<div class="site-info">
		<div class="payment-info">
			<span>Payments with</span>	
			<img src="<?php echo get_template_directory_uri(); ?>/assets/images/stripe-icon.svg" alt="payment">
		</div><!-- .payment-info -->
		<div class="copyright">
			<span>Promptfy® All Rights Reserved 2023.</span>
		</div>
		<?php if (has_nav_menu('footer')) : ?>
			<nav aria-label="<?php esc_attr_e('Secondary menu', 'twentytwentyone'); ?>" class="footer-navigation">
				<ul class="footer-navigation-wrapper">
					<?php
					wp_nav_menu(
						array(
							'theme_location' => 'footer',
							'items_wrap'     => '%3$s',
							'container'      => false,
							'depth'          => 1,
							'link_before'    => '<span>',
							'link_after'     => '</span>',
							'fallback_cb'    => false,
						)
					);
					?>
				</ul><!-- .footer-navigation-wrapper -->
			</nav><!-- .footer-navigation -->
		<?php endif; ?>
	</div><!-- .site-info -->
</footer><!-- #colophon -->

</div><!-- #page -->

<?php wp_footer(); ?>
<script src="<?php echo get_template_directory_uri(); ?>/assets/js/owl.carousel.js?0209"></script>
<script src="<?php echo get_template_directory_uri(); ?>/assets/js/jquery.nice-select.js?0209"></script>
<script src="<?php echo get_template_directory_uri(); ?>/assets/js/custom-select.js?0209"></script>
<script src="https://unpkg.com/@lottiefiles/lottie-player@latest/dist/lottie-player.js?0209"></script>
<script src="<?php echo get_template_directory_uri(); ?>/assets/js/sliders.js?0209"></script>
<script src="<?php echo get_template_directory_uri(); ?>/assets/js/base.js?0209"></script>
</body>

</html>