<?php
/**
 * Email Styles
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/emails/email-styles.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce\Templates\Emails
 * @version 7.4.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

// Load colors.
$bg        = get_option( 'woocommerce_email_background_color' );
$body      = get_option( 'woocommerce_email_body_background_color' );
$base      = get_option( 'woocommerce_email_base_color' );
$base_text = wc_light_or_dark( $base, '#202020', '#ffffff' );
$text      = get_option( 'woocommerce_email_text_color' );

// Pick a contrasting color for links.
$link_color = wc_hex_is_light( $base ) ? $base : $base_text;

if ( wc_hex_is_light( $body ) ) {
	$link_color = wc_hex_is_light( $base ) ? $base_text : $base;
}

$bg_darker_10    = wc_hex_darker( $bg, 10 );
$body_darker_10  = wc_hex_darker( $body, 10 );
$base_lighter_20 = wc_hex_lighter( $base, 20 );
$base_lighter_40 = wc_hex_lighter( $base, 40 );
$text_lighter_20 = wc_hex_lighter( $text, 20 );
$text_lighter_40 = wc_hex_lighter( $text, 40 );

// !important; is a gmail hack to prevent styles being stripped if it doesn't like something.
// body{padding: 0;} ensures proper scale/positioning of the email in the iOS native email app.
?>
body {
	background-color: <?php echo esc_attr( $bg ); ?>;
	padding: 0;
	text-align: center;
}

#outer_wrapper {
	background-color: <?php echo esc_attr( $bg ); ?>;
}

#wrapper {
	margin: 0 auto;
	padding: 50px 0;
	-webkit-text-size-adjust: none !important;
	width: 100%;
	max-width: 600px;
}

#template_container {
	background-color: <?php echo esc_attr( $body ); ?>;
	border-radius: 10px !important;
	overflow: hidden;
}

#template_header {
	background-color: <?php echo esc_attr( $base ); ?>;
	border-radius: 10px 10px 0 0 !important;
	color: <?php echo esc_attr( $base_text ); ?>;
	border-bottom: 0;
	font-weight: bold;
	line-height: 100%;
	vertical-align: middle;
	font-family: "Helvetica Neue", Helvetica, Roboto, Arial, sans-serif;
}

#template_header h1,
#template_header h1 a {
	color: <?php echo esc_attr( $base_text ); ?>;
	background-color: inherit;
}

#template_header_image img {
	margin-left: 0;
	margin-right: 0;
}

#template_footer td {
	padding: 0;
	border-radius: 10px;
}

#template_footer #credit {
	border: none;
	color: <?php echo esc_attr( $text_lighter_40 ); ?>;
	font-family: "Helvetica Neue", Helvetica, Roboto, Arial, sans-serif;
	font-size: 12px;
	line-height: 150%;
	text-align: center;
	padding: 24px 0;
}

#template_footer #credit p {
	margin: 0 0 16px;
	color: #FFFFFF60;
}

#body_content {
	background-color: <?php echo esc_attr( $body ); ?>;
}
table#addresses {
	margin-bottom: 0;
}
#body_content table {
	border: none;
}
#body_content table td {
	padding: 40px;
	border: none;
	border-bottom: 1px solid #FFFFFF40;
}

#body_content table td td {
	padding: 15px 0;
	line-height: 1em;
	border: none;
	border-bottom: 1px solid #FFFFFF40;
}
#body_content table th {
	padding-top: 0;
	font-weight: 300;
	padding: 15px 0 15px 0;
	border-bottom: 1px solid #FFFFFF40;
	text-align: left !important;
}
#body_content table thead th {
	padding: 15px 0;
	border: none;
	border-bottom: 1px solid #FFFFFF40;
	font-weight: 300;
	text-align: left !important;
}
#body_content table tfoot th {
	padding: 15px 0 15px 0;
	text-align: left !important;
}

#body_content table tfoot tr:last-of-type th,
#body_content table tfoot tr:last-of-type th + td {
	border-bottom: none;
	color: #FFFFFF; 
	padding-bottom: 0;
}
#body_content td ul.wc-item-meta {
	font-size: small;
	margin: 1em 0 0;
	padding: 0;
	list-style: none;
}

#body_content td ul.wc-item-meta li {
	margin: 0.5em 0 0;
	padding: 0;
}

#body_content td ul.wc-item-meta li p {
	margin: 0;
}

#body_content p {
	margin: 0 0 16px;
	color: #FFFFFF60;
}

#body_content_inner {
	color: #FFFFFF60;
	font-family: "Helvetica Neue", Helvetica, Roboto, Arial, sans-serif;
	font-size: 14px;
	line-height: 1.5em;
	text-align: left !important;
}
#body_content_inner p:last-child {
	margin: 0;
}
.td {
	border: none;
	color: #FFFFFF60;
	border-bottom: 1px solid #FFFFFF40;
	vertical-align: middle;
}

.address {
	padding: 0;
	color: #FFFFFF60;
	border: none;
	line-height: 27px;
	font-style: normal;
	font-size: 16px;
}

.text {
	color: <?php echo esc_attr( $text ); ?>;
	font-family: "Helvetica Neue", Helvetica, Roboto, Arial, sans-serif;
}

.link {
	color: #FD75BE !important;
}
a[href*="mailto:"] {
    color: #FD75BE !important;
}

#header_wrapper {
	padding: 36px 48px;
	display: block;
}

h1 {
	color: <?php echo esc_attr( $base ); ?>;
	font-family: "Helvetica Neue", Helvetica, Roboto, Arial, sans-serif;
	font-size: 30px;
	font-weight: 300;
	line-height: 1em;
	margin: 0;
	text-align: left;
}

h2 {
	color: #FFFFFF;
	display: block;
	font-family: "Helvetica Neue", Helvetica, Roboto, Arial, sans-serif;
	font-size: 20px;
	font-weight: bold;
	line-height: 1em;
	margin: 0 0 20px;
	text-align: left;
}

h3 {
	color: #FFFFFF;
	display: block;
	font-family: "Helvetica Neue", Helvetica, Roboto, Arial, sans-serif;
	font-size: 16px;
	font-weight: bold;
	line-height: 1em;
	margin: 16px 0 8px;
	text-align: left;
}

a {
	color: #FD75BE !important;
	font-weight: normal;
	text-decoration: underline;
}

img {
	border: none;
	display: inline-block;
	font-size: 14px;
	font-weight: bold;
	height: auto;
	outline: none;
	text-decoration: none;
	text-transform: capitalize;
	vertical-align: middle;
	margin-<?php echo is_rtl() ? 'left' : 'right'; ?>: 10px;
	max-width: 100%;
}

/**
 * Media queries are not supported by all email clients, however they do work on modern mobile
 * Gmail clients and can help us achieve better consistency there.
 */
@media screen and (max-width: 600px) {
	#header_wrapper {
		padding: 27px 36px !important;
		font-size: 24px;
	}

	#body_content table > tbody > tr > td {
		padding: 10px !important;
	}

	#body_content_inner {
		font-size: 10px !important;
	}
}
<?php
