<?php

/**
 * The template for displaying all single posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package WordPress
 * @subpackage Twenty_Twenty_One
 * @since Twenty Twenty-One 1.0
 * Template Name: Front
 */

get_header();

?>

<div class="home-page custom-page">
    <section class="presentation-wrap">
        <div class="presentation">
            <div class="title-wrap">
                <h1>Empower your 
                    <lottie-player class="lottie" src="https://assets4.lottiefiles.com/packages/lf20_Imjlj33KVc.json"  background="transparent"  speed="1" loop  autoplay></lottie-player>
                    AI 
                    <lottie-player class="lottie" src="https://assets9.lottiefiles.com/packages/lf20_Nt1O51RcKa.json"  background="transparent"  speed="1" loop  autoplay></lottie-player>
                    with our prompts
                </h1>
            </div>
            <p>Revolutionize your AI with Promptfy high-quality prompts. Fuel innovation and enhance accuracy today.</p>
            <div class="button-wrap">
                <a href="#view-more" class="main-btn big view-more">View more</a>
                <a href="<?php echo home_url() ?>/shop" class="main-btn alt big">Explore</a>
            </div>
        </div>
    </section>
    <section class="home-carousels" id="view-more">
        <div class="products filter">
            <div class="tabs tabs-carousel">
                <button class="tab active" id="btw" data-filter="btw">Latest</button>
                <button class="tab" id="btm" data-filter="btm">Featured</button>
            </div>
            <div class="products-carousel btw">
                <?php show_selected_products('latest'); ?>
            </div>
            <div class="products-carousel btm">
                <?php show_selected_products('featured'); ?>
            </div>
        </div>
        <div class="products small best-rated">
            <div class="title-carousel">
                <h3>Best rated</h3>
                <a class="main-link alt" href="">View all</a>
            </div>
            <div class="best-rated-carousel">
                <?php show_selected_products('best_rateds'); ?>
            </div>
        </div>
        <div class="products small best-sellers">
            <div class="title-carousel">
                <h3>Best sellers</h3>
                <a class="main-link alt" href="">View all</a>
            </div>
            <div class="best-rated-carousel">
                <?php show_selected_products('best_sellers'); ?>
            </div>
        </div>
    </section>
    <section class="banner big call-to-login">
        <div class="banner-content">
            <div class="banner-text">
                <h2>Your One-Stop-Shop for High-Quality AI Prompts</h2>
                <p>Promptfy is a platform that specializes in selling prompts for AI. With a vast library of high-quality datasets, Promptfy provides developers and researchers with the tools they need to create accurate and effective AI models. Whether you’re working on natural language processing, image recognition, or any other AI application, Promptfy has the prompts you need to succeed. <a class="main-link alt" href="">Learn more</a>.</p>
            </div>
            <div class="button-wrap">
                <a href="<?php echo home_url() ?>/my-account" class="main-btn darker big">Login</a>
                <a href="<?php echo home_url() ?>/my-account" class="main-btn alt darker big">Register</a>
            </div>
        </div>
    </section>
    <section class="banner small register-store">
        <div class="banner-content">
            <div class="banner-text">
                <h3>Create your prompt store now</h3>
                <p>Create your own prompt shop on Promptfy and earn money while contributing to AI development. Join our community of sellers today and showcase your expertise!</p>
            </div>
            <div class="button-wrap">
                <a href="<?php echo home_url() ?>/vendor-register" class="main-btn cta big">Register my store</a>
            </div>
        </div>
    </section>
</div>
<?php
get_footer();
?>