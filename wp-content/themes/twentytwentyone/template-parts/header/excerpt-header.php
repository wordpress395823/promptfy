<?php
/**
 
Displays the post header*
@package WordPress
@subpackage Twenty_Twenty_One
@since Twenty Twenty-One 1.0
*/

// Don't show the title if the post-format is aside or status.
$post_format = get_post_format();
if ( 'aside' === $post_format || 'status' === $post_format ) {
    return;
}
?>

<header class="entry-header">
    <?php
    ?>
    <div class="search-thumb-wrap">
    	<?php
    	if ( has_post_thumbnail() ) {
			the_post_thumbnail( 'thumbnail' );
		}

    	// Verifica se o post atual é um produto WooCommerce
    	//if ( function_exists( 'is_product' ) && is_product() ) {
    	    global $product;
			
		?>
    </div>
    <?php
    the_title( sprintf( '<p class="entry-title default-max-width">', esc_url( get_permalink() ) ), '</p>' );
    ?>
    <?php
if ( function_exists( 'is_product' ) && is_product() ) {
    global $product;

    // Verifica se o preço está disponível
    $price = $product->get_price();
    if ( $price ) {
        echo '<p class="price">' . wc_price( $price ) . '</p>';
    }
}
?>


	<?php
		// Verifica se o produto tem avaliações
        if ( $product->get_average_rating() > 0 ) :
            ?>
            <span class="rating">
                <?php echo wc_get_rating_html( $product->get_average_rating() ); ?>
            </span>
            <?php
        endif;
	?>

</header><!-- .entry-header -->