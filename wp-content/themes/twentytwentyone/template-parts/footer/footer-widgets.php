<?php
/**
 * Displays the footer widget area.
 *
 * @package WordPress
 * @subpackage Twenty_Twenty_One
 * @since Twenty Twenty-One 1.0
 */

if ( is_active_sidebar( 'sidebar-1' ) ) : ?>

	<aside class="widget-area pre-footer">
		<div class="about-brand">
			<div class="img-brand">
				<img src="<?php echo get_template_directory_uri(); ?>/assets/images/promptfy-logo.svg" alt="">
			</div>
			<div class="brand-description">
				<p>Create your own prompt shop on Promptfy and earn money while contributing to AI development. Join our community of sellers today and showcase your expertise!</p>
			</div>
		</div>
		<?php dynamic_sidebar( 'sidebar-1' ); ?>
	
	</aside><!-- .widget-area -->

	<?php
endif;
