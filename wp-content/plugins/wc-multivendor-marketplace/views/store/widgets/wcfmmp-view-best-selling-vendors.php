<?php
/**
 * The Template for displaying store sidebar best selling vendors.
 *
 * @package WCfM Markeplace Views Best Selling Vendors
 *
 * For edit coping this to yourtheme/wcfm/store/widgets
 *
 */
global $WCFM, $WCFMmp, $wpdb;

echo wp_kses_post(apply_filters('wcfmmp_before_widget_best_selling_vendor_list', '<div class="stores-explore enginners-carousel">'));

$vendor_count = count($vendors);

for ($i = 0; $i < $vendor_count; $i++) {
    $vendor = $vendors[$i];
    $vendor_id = absint($vendor['vendor_id']);

    $is_store_offline = get_user_meta($vendor_id, '_wcfm_store_offline', true);
    $is_disable_vendor = get_user_meta($vendor_id, '_disable_vendor', true);

    if ($is_store_offline || $is_disable_vendor) {
        continue;
    }

    $store_name = $WCFM->wcfm_vendor_support->wcfm_get_vendor_store_name_by_vendor($vendor_id);
    $store_logo = $WCFM->wcfm_vendor_support->wcfm_get_vendor_logo_by_vendor($vendor_id);

    if (!$store_logo) {
        $store_logo = esc_url($WCFMmp->plugin_url . 'assets/images/wcfmmp.png');
    }

    // Obter a contagem de produtos do vendedor
    $product_count = $wpdb->get_var($wpdb->prepare("
        SELECT COUNT(ID)
        FROM {$wpdb->prefix}posts
        WHERE post_type = 'product'
        AND post_status = 'publish'
        AND post_author = %d
    ", $vendor_id));

    // Obter os produtos do vendedor
    $products = $wpdb->get_results($wpdb->prepare("
        SELECT ID, post_title
        FROM {$wpdb->prefix}posts
        WHERE post_type = 'product'
        AND post_status = 'publish'
        AND post_author = %d
    ", $vendor_id));

    ?>
    <div class="enginners-wrap-itens">
        <div class="store-explore-block">
            <a href="<?php echo wcfmmp_get_store_url($vendor_id); ?>">
                <div class="store-img">
                    <img src="<?php echo esc_url($store_logo); ?>" alt="">
                </div>
                <div class="store-description">
                    <h4 class="store-name"><?php echo wp_kses_post($store_name); ?></h4>
                    <p class="product-count">
                        <?php echo sprintf(_n('%d prompt', '%d prompts', $product_count, 'text-domain'), $product_count); ?>
                    </p>
                    <?php
                        $total_rating = 0;
                        foreach ($products as $product) :
                            $product_id = $product->ID;
                            $average_rating = get_post_meta($product_id, '_wc_average_rating', true);
                            $total_rating += $average_rating;
                        endforeach;
                    ?>
                    <?php if ($product_count > 0 && $total_rating > 0) : ?>
                        <span class="tag rating"><span class="star-icon"></span><span><?php echo sprintf(__('%s', 'text-domain'), number_format($total_rating / $product_count, 1)); ?></span></span>
                    <?php endif; ?>
                </div>
            </a>
        </div>
    </div>
<?php }

echo wp_kses_post(apply_filters('wcfmmp_after_widget_best_selling_vendor_list', '</div>'));
?>
