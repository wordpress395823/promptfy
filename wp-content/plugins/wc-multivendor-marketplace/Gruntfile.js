// The "wrapper" function
module.exports = function( grunt ) {
    // Project and task configuration
    grunt.initConfig({
        pkg: grunt.file.readJSON("package.json"),
        uglify: {
			options: {
				banner: '/*! DO NOT EDIT THIS FILE. This file is a auto generated on <%= grunt.template.today("yyyy-mm-dd") %> */\n',
			},
			build: {
				files: [
					{
						expand: true,
						src: ["assets/js/**/*.js", "!assets/js/min/**", "!assets/js/carousel/**"],
						dest: ".",
						rename: function (dst, src) {
							// To keep the source js files and make new files in assets/js/min/:
							return dst + "/" + src.replace("assets/js/", "assets/js/min/");
						},
					},
				],
			},
		},
        cssmin: {
			options: {
				banner: '/*! DO NOT EDIT THIS FILE. This file is a auto generated on <%= grunt.template.today("yyyy-mm-dd") %> */\n',
			},
			target: {
				files: [
					{
						expand: true,
						src: ["assets/css/**/*.css", "!assets/css/min/**", "!assets/css/carousel/**"],
						dest: ".",
						rename: function (dst, src) {
							// To keep the source css files and make new files as in assets/css/min/:
							return dst + "/" + src.replace("assets/css/", "assets/css/min/")
						},
					},
				],
			},
		},
        makepot: {
			target: {
				options: {
					potComments:
						'/*! DO NOT EDIT THIS FILE. This file is a auto generated on <%= grunt.template.today("yyyy-mm-dd") %> */\nThis file is distributed under the same license as the WCFM - WooCommerce Multivendor Marketplace plugin.\n',
					potFilename: "<%= pkg.name %>.pot",
					potHeaders: {
						poedit: true, // Includes common Poedit headers.
						"x-poedit-keywordslist": true, // Include a list of all possible gettext functions.
					},
					domainPath: "lang",
					include: [
						"controllers/.*",
						"core/.*",
						"helpers/.*",
						"includes/.*",
						"views/.*",
						"wc-multivendor-marketplace-config.php",
						"wc-multivendor-marketplace.php",
					],
					type: "wp-plugin",
				},
			},
		},
        zip: {
			"./dist/<%= pkg.name %>.zip": [
				"./assets/**",
				"./controllers/**",
				"./core/**",
				"./helpers/**",
				"./includes/**",
				"./lang/**",
				"./views/**",
				"./readme.txt",
				"./wc-multivendor-marketplace-config.php",
				"./wc-multivendor-marketplace.php",
			],
		}
    });

    // Loading Grunt plugins and tasks
    grunt.loadNpmTasks("grunt-contrib-uglify");
    grunt.loadNpmTasks("grunt-contrib-cssmin");
	grunt.loadNpmTasks("grunt-wp-i18n");
	grunt.loadNpmTasks("grunt-zip");

    // Custom tasks
    //grunt.registerTask("default", ["uglify","cssmin","makepot","zip"]);
    grunt.registerTask("default", ["zip"]);
}
