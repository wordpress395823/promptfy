<?php

/**
 * The template for displaying all single posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package WordPress
 * @subpackage Twenty_Twenty_One
 * @since Twenty Twenty-One 1.0
 * Template Name: Hire
 */

get_header();

?>
<div class="hire-page custom-page">
    <section class="presentation-wrap">
        <div class="presentation">
            <div class="title-wrap">
                <h1>Hire an engineer 
					<lottie-player class="lottie" src="https://assets4.lottiefiles.com/packages/lf20_Imjlj33KVc.json"  background="transparent"  speed="1" loop  autoplay></lottie-player>
					to make 
					<lottie-player class="lottie" src="https://assets9.lottiefiles.com/packages/lf20_Nt1O51RcKa.json"  background="transparent"  speed="1" loop  autoplay></lottie-player>
					a prompt for you
				</h1>
            </div>
            <p>Commission custom prompts from top prompt engineers, saving time and empowering yourself to become a prompt engineer.</p>
        </div>
    </section>
	<section class="my-widget-area store-list-explore">
    	<?php dynamic_sidebar( 'my-widget-area' ); ?>
	</section>
</div>
<?php
get_footer();
?>